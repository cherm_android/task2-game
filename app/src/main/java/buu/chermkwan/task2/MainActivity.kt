package buu.chermkwan.task2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        game()
    }
    var Correct: Int = 0
    var Incorrect: Int = 0

    fun game() {
        val num1 = findViewById<TextView>(R.id.txtNum1)
        val num2 = findViewById<TextView>(R.id.txtNum2)
        val btnAns1 = findViewById<Button>(R.id.btnAns1)
        val btnAns2 = findViewById<Button>(R.id.btnAns2)
        val btnAns3 = findViewById<Button>(R.id.btnAns3)
        val txtTorF = findViewById<TextView>(R.id.txtTorF)
        val txtC = findViewById<TextView>(R.id.txtC)
        val txtOper = findViewById<TextView>(R.id.txtOper)
        val txtIn = findViewById<TextView>(R.id.txtIn)
        var randomNum1 = Random.nextInt(0, 10)
        var randomNum2 = Random.nextInt(0, 10)
        var randomOper = Random.nextInt(0, 3)
        val randomBtn = Random.nextInt(0, 2)

        num1.text = randomNum1.toString()
        num2.text = randomNum2.toString()

        var A = Random.nextInt(1, 18)
        var B = Random.nextInt(1, 18)

        var Answer: Int

        if (randomOper == 0) {
            Answer = randomNum1 + randomNum2
            txtOper.text = "+"
        } else if (randomOper == 1) {
            Answer = randomNum1 - randomNum2
            txtOper.text = "-"
        } else {
            Answer = randomNum1 * randomNum2
            txtOper.text = "x"
        }


        if (A == B) {
            A++
            B--
        } else if (Answer == A) {
            A++
        } else if (Answer == B) {
            B++
        }

        if (randomBtn == 0) {
            btnAns1.text = Answer.toString()
            btnAns2.text = A.toString()
            btnAns3.text = B.toString()
        } else if (randomBtn == 1) {
            btnAns2.text = Answer.toString()
            btnAns1.text = B.toString()
            btnAns3.text = A.toString()
        } else {
            btnAns3.text = Answer.toString()
            btnAns1.text = A.toString()
            btnAns3.text = B.toString()
        }

        btnAns1.setOnClickListener {
            if (btnAns1.text.toString() == Answer.toString()) {
                txtTorF.text = "Correct"
                Correct ++
                txtC.text = Correct.toString()
                game()
            } else {
                txtTorF.text = "Incorrect"
                Incorrect ++
                txtIn.text = Incorrect.toString()
                game()
            }
        }
        btnAns2.setOnClickListener {
            if (btnAns2.text.toString() == Answer.toString()) {
                txtTorF.text = "Correct"
                Correct ++
                txtC.text = Correct.toString()
                game()
            } else {
                txtTorF.text = "Incorrect"
                Incorrect ++
                txtIn.text = Incorrect.toString()
                game()
            }
        }
        btnAns3.setOnClickListener {
            if (btnAns3.text.toString() == Answer.toString()) {
                txtTorF.text = "Correct"
                Correct ++
                txtC.text = Correct.toString()
                game()
            } else {
                txtTorF.text = "Incorrect"
                Incorrect ++
                txtIn.text = Incorrect.toString()
                game()
            }
        }

    }
}